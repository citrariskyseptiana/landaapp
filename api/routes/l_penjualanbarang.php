<?php
/**
 * Ambil semua list user
 */
$app->get("/l_penjualanbarang/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $tanggal = $params['bulan'];
    $tanggal_int = strtotime($tanggal);
    $tanggal_awal = date("Y-m-01", $tanggal_int);
    $tanggal_akhir = date("Y-m-t", $tanggal_int);

    $db->select("
        m_barang.nama_barang AS barang_nama,
        m_barang.satuan AS barang_satuan,
        m_barang.id AS m_barang_id,
        t_penjualan_det.jumlah AS jumlah_barang,
        m_customer.nama_customer AS customer_nama,
        t_penjualan.tanggal AS penjualan_tanggal,
        t_penjualan_det.harga AS harga_satuan")
        ->from("t_penjualan")
        ->join("left join", "m_customer", "t_penjualan.m_customer_id=m_customer.id")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
        ->where("tanggal", ">=", $tanggal_awal)
        ->where("tanggal", "<=", $tanggal_akhir);

    if (isset($params["kelompok_customer"]) && !empty($params["kelompok_customer"])) {
        $db->where("m_customer.id", "=", $params["kelompok_customer"]);
    }
    if (isset($params["kelompok_barang"]) && !empty($params["kelompok_barang"])) {
        $db->where("m_barang.id", "=", $params["kelompok_barang"]);
    }

    $models = $db->findAll();
    $jumlahbrg = 0;
    $totalhrg = 0;
    $result = [];
    foreach ($models as $key => $value) {
        $SubTotal = $value->jumlah_barang * $value->harga_satuan;
        $jumlahbrg = $jumlahbrg + $value->jumlah_barang;
        $totalhrg = $totalhrg + $SubTotal;
        $models[$key]->SubTotal = $SubTotal;

        $result['jumlahbarang'] = $jumlahbrg;
        $result['totalhrg'] = $totalhrg;
    }
//    print_r($models);
//    die;

    $totalItem = $db->count();
    if (isset($params['is_export']) && $params['is_export'] == 1) {
//        print_r($models);
//        die;
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/rekap_barang_perbulan.xlsx");
        $sheet = $xls->getSheet(0);

        $sheet->getCell('A2')->setValue(date('F Y', strtotime($params['bulan'])));
        $index = 5;
        $no = 1;

        foreach ($models as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['penjualan_tanggal']);
            $sheet->getCell('C' . $index)->setValue($value['customer_nama']);
            $sheet->getCell('D' . $index)->setValue($value['barang_nama']);
            $sheet->getCell('E' . $index)->setValue($value['jumlah_barang']);
            $sheet->getCell('F' . $index)->setValue($value['barang_satuan']);
            $sheet->getCell('G' . $index)->setValue($value['harga_satuan']);
            $sheet->getCell('H' . $index)->setValue($value['SubTotal']);

            $index++;
        }
        $sheet->getCell('A' . $index)->setValue('TOTAL');
        $sheet->getCell('E' . $index)->setValue($result['jumlahbarang']);
        $sheet->getCell('H' . $index)->setValue($result['totalhrg']);
//        echo "excel";
//        die;
        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENJUALAN PER BULAN " . date("F Y", strtotime($params['bulan'])) . ".xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;
    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("laporan/penjualan.html", [
            'data' => $models,
            'total' => $result,
            'month' => $params,
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    }
//    print_r($models);
//    die;
    return successResponse($response, ["rincian" => $models, "totalsemua" => $result]);
});

$app->get("/l_penjualanbarang/customer", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_customer");

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});
$app->get("/l_penjualanbarang/barang", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_barang");

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});
