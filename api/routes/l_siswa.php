<?php
/**
 * Ambil semua list user
 */
$app->get("/l_siswa/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
        m_siswa.*, 
        m_sekolah.nama_sekolah AS sekolah_nama,
        m_sekolah.alamat_sekolah AS sekolah_alamat,
        m_sekolah.no_telepon AS sekolah_telepon,
        m_siswa.nama_siswa AS siswa_nama,
        m_siswa.nis AS siswa_nis,
        m_siswa.tanggal_lahir AS siswa_tanggal_lahir")
        ->from("m_sekolah")
        ->join("left join", "m_siswa", "m_siswa.m_sekolah_id=m_sekolah.id");
//        ->where("m_jurusan.is_deleted", "=", 0);


    if (isset($params["kelompok_sekolah"]) && !empty($params["kelompok_sekolah"])) {
        $db->where("m_sekolah.jenis_sekolah", "=", $params["kelompok_sekolah"]);
    }
//    print_r($params);
//    die;

    $models = $db->findAll();
//    print_r($models);
//    die;
    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_sekolah_id] ["m_sekolah_id"] = $value->m_sekolah_id;
        $result[$value->m_sekolah_id] ["sekolah_nama"] = $value->sekolah_nama;
        $result[$value->m_sekolah_id] ["sekolah_alamat"] = $value->sekolah_alamat;
        $result[$value->m_sekolah_id] ["sekolah_telepon"] = $value->sekolah_telepon;
        $result[$value->m_sekolah_id] ['listSiswa'] [] = $value;

    }

//    print_r($result);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "totalItems" => $totalItem]);
});
$app->get("/l_siswa/sekolah", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_sekolah")
        ->where("m_sekolah.jenis_sekolah", "=", "jenis_sekolah");

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});