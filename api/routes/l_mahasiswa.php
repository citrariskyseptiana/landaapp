<?php
/**
 * Ambil semua list user
 */
$app->get("/l_mahasiswa/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
        m_jurusan.*,
        m_jurusan.nama AS nama_jurusan,
        mahasiswa.nim AS mahasiswa_nim,
        mahasiswa.tanggal_lahir AS mahasiswa_tanggal_lahir,
        mahasiswa.nama AS nama_mahasiswa,
        m_kelas.nama_kelas AS kelas_nama,
        t_kelompok_kelas_id AS kelompok_kelas")
        ->from("m_jurusan")
        ->join("left join", "t_kelompok_kelas", "t_kelompok_kelas.m_jurusan_id=m_jurusan.id")
        ->join("left join", "m_kelas", "t_kelompok_kelas.m_kelas_id=m_kelas.id")
        ->join("left join", "t_kelompok_kelas_det", "t_kelompok_kelas_det.t_kelompok_kelas_id=t_kelompok_kelas.id")
        ->join("left join", "mahasiswa", "mahasiswa.id=t_kelompok_kelas_det.mahasiswa_id")
        ->where("m_jurusan.is_deleted", "=", 0);

    if (isset($params["kelompok_jurusan"]) && !empty($params["kelompok_jurusan"])) {
        $db->where("m_jurusan.id", "=", $params["kelompok_jurusan"]);
    }
    $models = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
$app->get("/l_mahasiswa/jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_jurusan");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});