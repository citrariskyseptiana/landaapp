<?php

/**
 * Ambil semua list user
 */
$app->get("/l_jurusan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
        m_jurusan.*,
        m_jurusan.nama AS jurusan_nama,
        mahasiswa.nim AS mahasiswa_nim,
        mahasiswa.tanggal_lahir AS mahasiswa_tanggal_lahir,
        mahasiswa.alamat AS mahasiswa_alamat,
        mahasiswa.nama AS mahasiswa_nama,
        m_kelas.nama_kelas AS kelas_nama,
        t_kelompok_kelas.m_jurusan_id AS kelompok_jurusan_id,
        m_kelas.id AS m_kelas_id,
        m_jurusan.alamat AS jurusan_alamat,
        m_jurusan.no_telepon AS jurusan_telepon,
        t_kelompok_kelas_id AS kelompok_kelas")
        ->from("m_jurusan")
        ->join("left join", "t_kelompok_kelas", "t_kelompok_kelas.m_jurusan_id=m_jurusan.id")
        ->join("left join", "m_kelas", "t_kelompok_kelas.m_kelas_id=m_kelas.id")
        ->join("left join", "t_kelompok_kelas_det", "t_kelompok_kelas_det.t_kelompok_kelas_id=t_kelompok_kelas.id")
        ->join("left join", "mahasiswa", "mahasiswa.id=t_kelompok_kelas_det.mahasiswa_id");
//        ->where("m_jurusan.is_deleted", "=", 0);


    if (isset($params["kelompok_jurusan"]) && !empty($params["kelompok_jurusan"])) {
        $db->where("m_jurusan.id", "=", $params["kelompok_jurusan"]);
    }
    if (isset($params["kelompok_kelas"]) && !empty($params["kelompok_kelas"])) {
        $db->where("m_kelas.id", "=", $params["kelompok_kelas"]);
//        print_r($params);
//        die;
    }

    $models = $db->findAll();
//    print_r($models);
//    die;
    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_kelas_id] ["m_kelas_id"] = $value->m_kelas_id;
        $result[$value->m_kelas_id] ["jurusan_nama"] = $value->jurusan_nama;
        $result[$value->m_kelas_id] ["jurusan_alamat"] = $value->jurusan_alamat;
        $result[$value->m_kelas_id] ["jurusan_telepon"] = $value->jurusan_telepon;
        $result[$value->m_kelas_id] ["kelas_nama"] = $value->kelas_nama;
        if (!empty($value->mahasiswa_nim)) {
            $result[$value->m_kelas_id] ['listMhs'] [] = $value;
        }
    }

//    print_r($result);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "totalItems" => $totalItem]);
});
$app->get("/l_jurusan/jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
        ->from("m_jurusan");

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});
$app->get("/l_jurusan/kelas/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute("id");

    $db->select("m_kelas.*")
        ->from("m_kelas")
        ->join("left join", "m_jurusan", "m_kelas.m_jurusan_id=m_jurusan.id")
        ->where("m_kelas.m_jurusan_id", "=", $id);

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});