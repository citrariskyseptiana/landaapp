<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama_sekolah"  => "required",
        "alamat_sekolah"  => "required",
        "no_telepon"  => "required",
        "jenis_sekolah"  => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil detail m sekolah
 */
$app->get("/m_sekolah/view", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_siswa")
        ->where("m_sekolah_id", "=", $params["m_sekolah_id"]);
    $models     = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua m sekolah
 */
$app->get("/m_sekolah/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_sekolah");
//        ->where("is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m sekolah
 */
$app->post("/m_sekolah/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
//    print_r($data);
//    die;
//    $data['data']['tanggal_lahir'] = date("Y-m-d", strtotime($data['data']['tanggal_lahir']));
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            if (isset($data["data"]["id"])) {
                $model = $db->update("m_sekolah", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("m_siswa", ["m_sekolah_id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("m_sekolah", $data["data"]);
            }
            /**
             * Simpan detail
             */
            if(isset($data["detail"]) && !empty($data["detail"])){
                foreach($data["detail"] as $key => $val){

                    $detail["id"] = isset($val["id"]) ? $val["id"] : '';
                    $detail["nis"] = isset($val["nis"]) ? $val["nis"] : '';
                    $detail["nama_siswa"] = isset($val["nama_siswa"]) ? $val["nama_siswa"] : '';
                    $detail["tanggal_lahir"] = isset($val["tanggal_lahir"]) ? $val["tanggal_lahir"] : '';
                    $detail["jenis_sekolah"] = isset($val["jenis_sekolah"]) ? $val["jenis_sekolah"] : '';
//                    print_r($detail);
//                    die;
                    $detail["m_sekolah_id"] = $model->id;
                    $db->insert("m_siswa", $detail);
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m sekolah
 */
$app->post("/m_sekolah/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_sekolah", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
